import numpy as np
from .Camion import Camion
from .Surtidor import Surtidor
from .Utiles import _mean_confidence_interval, _exportarGrafico
from .Config import EXPERIMENTOS, TIEMPO_SIMULACION, CORRIDAS, CANTIDAD_SURTIDORES


class Punto1:
    def __init__(self):
        self.cantidad_experimentos = EXPERIMENTOS
        self.cantidad_corridas = CORRIDAS
        self.cantidad_surtidores = CANTIDAD_SURTIDORES
        self.tiempo_simulacion = TIEMPO_SIMULACION
        self.cola_eventos = []
        self.surtidores = []
        self.experimentos = []
        self.reloj_global = 0

    def inicializar(self):
        self.reloj_global = 0
        self.cola_eventos = []
        self.surtidores = []
        self.generar_camiones()
        self.inicializar_surtidores()

    def inicializar_surtidores(self):
        fx_tiempos = [
            lambda x: np.random.normal(18, 4),
            lambda x: np.random.exponential(15),
            lambda x: np.random.exponential(16),
            lambda x: np.random.normal(14, 3)
        ]

        if self.cantidad_surtidores == 5:
            fx_tiempos.append(
                lambda x: np.random.normal(19, 5)
            )

        for i in range(0, self.cantidad_surtidores):
            self.surtidores.append(Surtidor(i+1, fx_tiempos[i]))

    def generar_camiones(self):
        contador = 0
        acumulador = 0

        while True:
            contador += 1
            tiempo_llegada = np.random.exponential(15)

            if tiempo_llegada + acumulador <= self.tiempo_simulacion:
                acumulador = acumulador + tiempo_llegada

                self.agregar_a_la_cola_de_eventos(
                    {
                        'camion': Camion(contador, acumulador),
                        'reloj': acumulador,
                        'tipo': 'LLEGADA'
                    }
                )
            else:
                break

    def surtidores_ocupados(self):
        for surtidor in self.surtidores:
            if not surtidor.ocupado():
                return False

        return True

    def obtener_surtidor_desocupado(self):
        for surtidor in self.surtidores:
            if not surtidor.ocupado():
                return surtidor

    def agregar_a_la_cola_de_eventos(self, evento):
        self.cola_eventos.append(evento)
        self.reordenar_cola_de_eventos()

    def reordenar_cola_de_eventos(self):
        self.cola_eventos = sorted(self.cola_eventos, key=lambda k: k['reloj'])

    def formatear_evento(self, evento):
        if evento['tipo'] == 'LLEGADA':
            return "Llegada de camion "+str(evento['camion'].id)
        else:
            return "Fin de atención de camion "+str(evento['surtidor'].camion.id)

    def procesar_evento(self, evento):
        if evento['tipo'] == 'LLEGADA':
            if not self.surtidores_ocupados():
                surtidor = self.obtener_surtidor_desocupado()
                self.agregar_a_la_cola_de_eventos(surtidor.ocupar(evento['camion'], self.reloj_global))
            else:
                evento['camion'].aumentar_espera()
                evento['reloj'] += 1
                self.reordenar_cola_de_eventos()
                #print("ESPERANDO")
        else:  # Tipo = SALIDA
            evento['surtidor'].desocupar(self.reloj_global)

    def obtener_siguiente_evento(self):
        return next((evento for evento in self.cola_eventos if evento['reloj'] > self.reloj_global), None)

    def generar_reportes(self, esperas_por_experimentos, corridas):
        print("Resultados: ")
        print("# El intervalo de confianza para los promedios de esperas por experimento. Con el 99% de probabilidad es de: ")
        conf_d, conf_d_izq, conf_d_der = _mean_confidence_interval(esperas_por_experimentos)
        print("+ Confianza: " + str(conf_d) + " (Izq: " + str(conf_d_izq) + " - Der: " + str(conf_d_der) + ")")
        print("# Promedio de esperas: ")
        print("+ El promedio de esperas de cada experimento fue: "+str(esperas_por_experimentos))
        print("+ El promedio general de las esperas es: "+str(sum(esperas_por_experimentos) / len(esperas_por_experimentos)))
        print("# Porcentaje de ocupación de cada surtidor: ")

        surtidores = [0, 0, 0, 0, 0] # Para 5 surtidores
        # surtidores = [0, 0, 0, 0] # Para 4 surtidores
        for corrida in corridas:
            surtidores[0] += corrida['surtidores'][0].tiempo_atencion
            surtidores[1] += corrida['surtidores'][1].tiempo_atencion
            surtidores[2] += corrida['surtidores'][2].tiempo_atencion
            surtidores[3] += corrida['surtidores'][3].tiempo_atencion
            surtidores[4] += corrida['surtidores'][4].tiempo_atencion
        for i in range(len(surtidores)):
            print("+ Utilización del Surtidor "+str(i+1)+": "+str(surtidores[i])+" ("+str(surtidores[i]*100/sum(surtidores))+"%)")
        print("# Tiempo promedio de espera por experimentos (Histograma): ")
        _exportarGrafico(esperas_por_experimentos, '', '', 'Punto1e4('+str(CANTIDAD_SURTIDORES)+'-Surt).png')
        print("+ El gráfico del tiempo promedio de espera de los experimentos fue exportado en la carpeta de gráficos")

    def ejecutar(self):
        self.experimentos = []
        esperas_por_experimentos = []

        for _ in range(self.cantidad_experimentos):
            corridas = []
            esperas_por_corrida = []

            for _ in range(self.cantidad_corridas):
                self.inicializar()

                while True:
                    evento = self.obtener_siguiente_evento()

                    if evento is not None:
                        # print("[Cola Eventos] - Ejecutando evento: "+self.formatear_evento(evento))

                        self.reloj_global = evento['reloj']
                        self.procesar_evento(evento)
                    else:
                        break

                corridas.append({
                    'cola_eventos': self.cola_eventos,
                    'surtidores': self.surtidores
                })

                esperas = [evento['camion'].tiempo_espera for evento in self.cola_eventos if evento['tipo'] == 'LLEGADA']
                esperas_por_corrida.append(sum(esperas) / len(esperas))

            self.experimentos.append(corridas)
            esperas_por_experimentos.append(sum(esperas_por_corrida) / len(esperas_por_corrida))

        self.generar_reportes(esperas_por_experimentos, corridas)
