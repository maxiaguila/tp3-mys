class Surtidor:
    def __init__(self, id, fx_tiempo_atencion):
        self.id = id
        self.camion = None
        self.atenciones = 0
        self.tiempo_atencion = 0
        self.fx_tiempo_atencion = fx_tiempo_atencion
        self.inicio_atencion = 0
        self.historico = []

    def ocupar(self, camion, reloj_global):
        self.atenciones += 1
        self.camion = camion
        self.tiempo_atencion = self.fx_tiempo_atencion('x')
        self.inicio_atencion = reloj_global
        # print("Surtidor "+str(self.id)+": Atendiendo al camion "+str(self.camion.id))

        return {
            'surtidor': self,
            'reloj': self.tiempo_atencion + reloj_global,
            'tipo': 'SALIDA'
        }

    def desocupar(self, reloj_global):
        self.camion.registrar_salida(reloj_global)
        # print("Surtidor "+str(self.id)+": Dejando de atender al camion "+str(self.camion.id))
        self.camion = None
        self.agregar_historico(reloj_global)

    def ocupado(self):
        return self.camion is not None

    def agregar_historico(self, reloj_global):
        self.historico.append(reloj_global - self.inicio_atencion)
        self.inicio_atencion = 0
