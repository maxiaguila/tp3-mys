import numpy
import scipy.stats as st
import seaborn as sns
import matplotlib.pyplot as plt


def _mean_confidence_interval(data, confidence=0.99):
    a = 1.0 * numpy.array(data)
    n = len(a)
    m, se = numpy.mean(a), st.sem(a)
    h = se * st.expon.ppf((1 + confidence) / 2., n - 1)
    return m, m - h, m + h


def _exportarGrafico(datos, leyenda_x, leyenda_y, titulo_archivo):
    ax = sns.distplot(datos,
                      kde=True,
                      bins=100,
                      color='skyblue',
                      hist_kws={
                          "linewidth": 15,
                          'alpha': 1
                      }
                      )
    ax.set(xlabel=leyenda_x, ylabel=leyenda_y)
    plt.savefig('Graficos/'+titulo_archivo)
    plt.clf()
