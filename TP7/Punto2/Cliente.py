class Cliente:
    def __init__(self, id, reloj_llegada):
        self.id = id
        self.tiempo_espera = 0
        self.reloj_llegada = 0
        self.reloj_salida = 0
        self.registrar_llegada(reloj_llegada)

    def aumentar_espera(self, espera=1):
        self.tiempo_espera += espera

    def registrar_llegada(self, reloj_llegada):
        self.reloj_llegada = reloj_llegada

    def registrar_salida(self, reloj_salida):
        self.reloj_salida = reloj_salida
