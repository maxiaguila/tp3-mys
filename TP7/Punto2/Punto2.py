import numpy as np
from .Cliente import Cliente
from .Caja import Caja
from .Utiles import _mean_confidence_interval, _exportarGrafico
from .Config import EXPERIMENTOS, TIEMPO_SIMULACION, CANTIDAD_CAJAS


class Punto2:
    def __init__(self):
        self.cantidad_experimentos = EXPERIMENTOS
        self.cantidad_cajas = CANTIDAD_CAJAS
        self.tiempo_simulacion = TIEMPO_SIMULACION
        self.cola_eventos = []
        self.cajas = []
        self.reloj_global = 0

    def inicializar(self):
        self.reloj_global = 0
        self.cola_eventos = []
        self.cajas = []
        self.generar_clientes()
        self.inicializar_cajas()

    def inicializar_cajas(self):
        fx_tiempos = [
            lambda x: np.random.normal(15, 3),
            lambda x: np.random.exponential(12)
        ]

        if self.cantidad_cajas == 3:
            fx_tiempos.append(
                lambda x: np.random.normal(14, 6)
            )

        for i in range(0, self.cantidad_cajas):
            self.cajas.append(Caja(i + 1, fx_tiempos[i]))

    def generar_clientes(self):
        contador = 0
        acumulador = 0

        while True:
            contador += 1
            tiempo_llegada = np.random.exponential(15)

            if tiempo_llegada + acumulador <= self.tiempo_simulacion:
                acumulador = acumulador + tiempo_llegada

                self.agregar_a_la_cola_de_eventos(
                    {
                        'cliente': Cliente(contador, acumulador),
                        'reloj': acumulador,
                        'tipo': 'LLEGADA'
                    }
                )
            else:
                break

    def cajas_ocupadas(self):
        for caja in self.cajas:
            if not caja.ocupada():
                return False

        return True

    def obtener_caja_desocupada(self):
        for caja in self.cajas:
            if not caja.ocupada():
                return caja

    def agregar_a_la_cola_de_eventos(self, evento):
        self.cola_eventos.append(evento)
        self.reordenar_cola_de_eventos()

    def reordenar_cola_de_eventos(self):
        self.cola_eventos = sorted(self.cola_eventos, key=lambda k: k['reloj'])

    def formatear_evento(self, evento):
        if evento['tipo'] == 'LLEGADA':
            return "Llegada de cliente "+str(evento['cliente'].id)
        else:
            return "Fin de atención de cliente "+str(evento['cajas'].camion.id)

    def procesar_evento(self, evento):
        if evento['tipo'] == 'LLEGADA':
            if not self.cajas_ocupadas():
                caja = self.obtener_caja_desocupada()
                self.agregar_a_la_cola_de_eventos(caja.ocupar(evento['cliente'], self.reloj_global))
            else:
                evento['cliente'].aumentar_espera()
                evento['reloj'] += 1
                self.reordenar_cola_de_eventos()
        else:  # Tipo = SALIDA
            evento['caja'].desocupar(self.reloj_global)

    def obtener_siguiente_evento(self):
        return next((evento for evento in self.cola_eventos if evento['reloj'] > self.reloj_global), None)

    def generar_reportes(self, esperas_por_experimentos, utilizacion_cajas_experimentos):
        print("Resultados: ")
        print("# El intervalo de confianza para los promedios de esperas por experimento. Con el 99% de probabilidad es de: ")
        conf_d, conf_d_izq, conf_d_der = _mean_confidence_interval(esperas_por_experimentos)
        print("+ Confianza: " + str(conf_d) + " (Izq: " + str(conf_d_izq) + " - Der: " + str(conf_d_der) + ")")
        print("# Promedio de esperas: ")
        print("+ El promedio de esperas de cada experimento fue: "+str(esperas_por_experimentos))
        print("+ El promedio general de las esperas es: "+str(sum(esperas_por_experimentos) / len(esperas_por_experimentos)))
        print("# Porcentaje de ocupación de cada caja: ")
        total_usos = sum([sum(experimento) for experimento in utilizacion_cajas_experimentos])
        for i in range(len(self.cajas)):
            total_utilizacion_caja_i = sum([experimento[i] for experimento in utilizacion_cajas_experimentos])
            print("+ Utilización de la Caja " + str(i+1) + ": " + str(total_utilizacion_caja_i) + " (" + str(total_utilizacion_caja_i * 100 / total_usos) + "%)")
        print("# Tiempo promedio de espera por experimentos (Histograma): ")
        _exportarGrafico(esperas_por_experimentos, '', '', 'Punto2e4(' + str(CANTIDAD_CAJAS) + '-Caja).png')
        print("+ El gráfico del tiempo promedio de espera de los experimentos fue exportado en la carpeta de gráficos")

    def ejecutar(self):
        esperas_por_experimentos = []
        utilizacion_cajas_experimentos = []

        for _ in range(self.cantidad_experimentos):  # 365 DIAS
            self.inicializar()

            while True:
                evento = self.obtener_siguiente_evento()

                if evento is not None:
                    # print("[Cola Eventos] - Ejecutando evento: "+self.formatear_evento(evento))

                    self.reloj_global = evento['reloj']
                    self.procesar_evento(evento)
                else:
                    break

            esperas = [evento['cliente'].tiempo_espera for evento in self.cola_eventos if evento['tipo'] == 'LLEGADA']
            esperas_por_experimentos.append(sum(esperas) / len(esperas))
            utilizacion_cajas_experimentos.append(list(map(lambda x: x.atenciones, self.cajas)))

        self.generar_reportes(esperas_por_experimentos, utilizacion_cajas_experimentos)
