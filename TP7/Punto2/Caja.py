class Caja:
    def __init__(self, id, fx_tiempo_atencion):
        self.id = id
        self.cliente = None
        self.atenciones = 0
        self.tiempo_atencion = 0
        self.fx_tiempo_atencion = fx_tiempo_atencion
        self.inicio_atencion = 0
        self.historico = []

    def ocupar(self, cliente, reloj_global):
        self.atenciones += 1
        self.cliente = cliente
        self.tiempo_atencion = self.fx_tiempo_atencion('x')
        self.inicio_atencion = reloj_global
        # print("Caja "+str(self.id)+": Atendiendo al cliente "+str(self.camion.id))

        return {
            'caja': self,
            'reloj': self.tiempo_atencion + reloj_global,
            'tipo': 'SALIDA'
        }

    def desocupar(self, reloj_global):
        self.cliente.registrar_salida(reloj_global)
        # print("Caja "+str(self.id)+": Dejando de atender al cliente "+str(self.camion.id))
        self.cliente = None
        self.agregar_historico(reloj_global)

    def ocupada(self):
        return self.cliente is not None

    def agregar_historico(self, reloj_global):
        self.historico.append(reloj_global - self.inicio_atencion)
        self.inicio_atencion = 0
