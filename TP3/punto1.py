from scipy.stats import expon
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

data_expon = expon.rvs(scale=1,loc=0,size=100)

print("El conjunto de datos correspondiente a una distribucion exponencial:")
print(data_expon)
print("La Media es ->", numpy.mean(data_expon))
print("El Desvio Standar es ->", numpy.nanstd(data_expon))
print("La Varianza es ->", numpy.var(data_expon))

ax = sns.distplot(data_expon,
                  kde=True,
                  bins=100,
                  color='skyblue',
                  hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Exponencial', ylabel='Frecuencia')
plt.savefig('punto1py.png')
