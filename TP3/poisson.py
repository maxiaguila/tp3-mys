from scipy.stats import poisson
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

data_poisson = poisson.rvs(mu=5, size=1000)

ax = sns.distplot(data_poisson, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Poisson', ylabel='Frecuencia')
plt.savefig('poisson.png')


