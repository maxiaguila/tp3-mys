from scipy.stats import norm
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

data_norm = norm.rvs(scale=1,loc=0,size=1000)
print("Media-> ", numpy.mean(data_norm))
print("Desvio Estandar-> ", numpy.nanstd(data_norm))

ax = sns.distplot(data_norm, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Normal', ylabel='Frecuencia')
plt.savefig('normal.png')

