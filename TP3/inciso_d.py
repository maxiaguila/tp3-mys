import scipy.stats as st
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * numpy.array(data)
    n = len(a)
    m, se = numpy.mean(a), st.sem(a)
    h = se * st.expon.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h

# lambda 2
# scale = 1/lamda -> scale=0.5
data_expon = st.expon.rvs(scale=0.5,loc=0,size=1000)
print("Varianza-> ", numpy.var(data_expon)) # Variancia = 1/2^2
print("Media-> ", numpy.mean(data_expon))
print("Desvio Estandar->", numpy.nanstd(data_expon))
m,izquierda,derecha = mean_confidence_interval(data_expon, 0.9977)
print("Media para el Intervalo de Confianza 99%: ", m)
print("Intervalo de Confianza Izquierdo -3: ", izquierda)
print("Intervalo de Confianza Derecho +3: ", derecha)

ax = sns.distplot(data_expon, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Exponencial', ylabel='Frecuencia')
plt.savefig('exponencial.png')


