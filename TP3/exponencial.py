from scipy.stats import expon
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

#scale = 1/lamda -> scale=4
data_expon = expon.rvs(scale=4,loc=0,size=1000)
print("Varianza-> ", numpy.var(data_expon)) # Variancia = 1/lamda^2 
print("Media-> ", numpy.mean(data_expon))
print("Desvio Estandar->", numpy.nanstd(data_expon))

ax = sns.distplot(data_expon, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Exponencial', ylabel='Frecuencia')
plt.savefig('exponencial.png')


