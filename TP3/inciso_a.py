from scipy.stats import expon, uniform
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

def exponenciar(lam, x):
    ret = []

    for i in x:
        ret.append(-lam**-1 * np.log(i))

    return ret

n = 1000
start = 0
width = 1
datos = uniform.rvs(size=n, loc = start, scale=width)
print(datos)

transformado = exponenciar(8, datos)

print(transformado)

print("La media del transformado es: ", np.mean(transformado))
print("La varianza del transformado es: ", np.var(transformado))

ax = sns.distplot(transformado, bins=100, kde=True, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Uniforme', ylabel='Frecuenccia')
plt.savefig('inciso_a.png')

