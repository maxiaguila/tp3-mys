from scipy.stats import uniform
import numpy
import seaborn as sns
import matplotlib.pyplot as plt

n = 1000
start = 0
width = 1
data_uniform = uniform.rvs(size=n, loc = start, scale=width)

ax = sns.distplot(data_uniform, bins=100, kde=True, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Distribucion Uniforme', ylabel='Frecuenccia')
plt.savefig('uniforme.png')

