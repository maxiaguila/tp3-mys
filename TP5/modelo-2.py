import numpy
import seaborn
import scipy.stats as st
import matplotlib.pyplot as plt


NUM_EXPERIMENTOS = 30
MAX_CORRIDAS = 100
duracion_proyectos_corrida = []
duracion_promedio_experimento = []
promedio_camino_critico_acceso_medio = []
promedio_camino_critico_acceso_inferior = []
promedio_camino_critico_acceso_superior = []
histograma_corridas = []

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * numpy.array(data)
    n = len(a)
    m, se = numpy.mean(a), st.sem(a)
    h = se * st.expon.ppf((1 + confidence) / 2., n-1)
    return m, m-h, m+h


for _ in range(NUM_EXPERIMENTOS):
    camino_critico_acceso_superior = []
    camino_critico_acceso_medio = []
    camino_critico_acceso_inferior = [] 
    for _ in range(MAX_CORRIDAS):
        tarea_a = numpy.random.uniform(2,4)
        tarea_b = numpy.random.uniform(3,6)
        tarea_c = numpy.random.uniform(2,5)
        tarea_d = numpy.random.uniform(3,6)
        tarea_e = numpy.random.uniform(2,5)
        tarea_f = numpy.random.uniform(4,8)
        tarea_g = numpy.random.uniform(3,7)

        acceso_superior = [tarea_a, tarea_b, tarea_c]
        acceso_medio = [tarea_a, tarea_d, tarea_e, tarea_f]
        acceso_inferior = [tarea_a, tarea_d, tarea_e, tarea_f]

        # La duracion de cada acceso es la sumatoria de la duracion de cada una de las tareas
        duracion_acceso_superior = sum(acceso_superior)
        duracion_acceso_medio = sum(acceso_medio)
        duracion_acceso_inferior = sum(acceso_superior)

        corrida = []
        corrida.append(duracion_acceso_superior)
        corrida.append(duracion_acceso_medio)
        corrida.append(duracion_acceso_inferior) 

        # La duracon del proyecto es la sumatoria de todas las actividades, o de los accesos
        duracion_proyecto = sum(corrida)

        # El camino critico es el acceso que mas duracion tiene
        camino_critico_acceso_superior.append(duracion_acceso_superior)
        camino_critico_acceso_medio.append(duracion_acceso_medio)
        camino_critico_acceso_inferior.append(duracion_acceso_inferior)

        # Array que contendra todas las duraciones de los proyectos en cada corrida para luego sacar un promedio.
        duracion_proyectos_corrida.append(duracion_proyecto)

        histograma_corridas.append(duracion_proyecto)
    
    # Promedio por experimento 
    duracion_promedio_experimento.append(sum(duracion_proyectos_corrida)/MAX_CORRIDAS)

    promedio_camino_critico_acceso_superior.append(sum(camino_critico_acceso_superior)/100)
    promedio_camino_critico_acceso_medio.append(sum(camino_critico_acceso_medio)/100)
    promedio_camino_critico_acceso_inferior.append(sum(camino_critico_acceso_inferior)/100)

    # Intervalo de confianza
    m,izquierda,derecha = mean_confidence_interval(duracion_proyectos_corrida, 0.9977)

    duracion_proyectos_corrida.clear()

print("RESULTADOS-------------------------------------------------------------------")    
print("La duracion promedio de cada experimento es: ", duracion_promedio_experimento)
print("-----------------------------------------------------------------------------")
print("Porcentaje de criticidad del acceso superior: ", promedio_camino_critico_acceso_superior)
print("-----------------------------------------------------------------------------")
print("Porcentaje de criticidad del acceso medio: ", promedio_camino_critico_acceso_medio)
print("-----------------------------------------------------------------------------")
print("porcentaje de criticidad del acceso inferior: ", promedio_camino_critico_acceso_inferior)
print("-----------------------------------------------------------------------------")
print("IC---------------------------------------------------------------------------")
print("Medio: ", m)
print("Izquierda: ", izquierda)
print("Derecha: ", derecha)

ax = seaborn.distplot(histograma_corridas, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
ax.set(xlabel='Corridas', ylabel='Tiempo')
plt.savefig('Histograma_Corridas_modelo-2.png')
plt.clf()
sx = seaborn.distplot(duracion_promedio_experimento, kde=True, bins=100, color='skyblue', hist_kws={"linewidth": 15,'alpha':1})
sx.set(xlabel='Experimentos', ylabel='Tiempo')
plt.savefig('Histograma_Experimentos_modelo-2.png')
